#program name
TARGET = game
#build file
OBJDIR = build

# colors
Color_Off='\033[0m'
Black='\033[1;30m'
Red='\033[1;31m'
Green='\033[1;32m'
Yellow='\033[1;33m'
Blue='\033[1;34m'
Purple='\033[1;35m'
Cyan='\033[1;36m'
White='\033[1;37m'



SOURCES = $(wildcard *.cpp)
OBJECTS = $(SOURCES:%.cpp=$(OBJDIR)/%.o)

LFLAGS = -lz -lm `pkg-config --libs freealut` -lbluetooth -std=c++17 -lGLU -lGL -lX11 -lalut
LINKER = g++ -o


CFLAGS = -lz -lm `pkg-config --libs freealut` -lbluetooth -std=c++17 -lGLU -lGL -lX11 -lalut
CC = g++

$(TARGET): $(OBJECTS)
	@$(LINKER) $@ $^ $(LFLAGS)
	@echo $(Yellow)"Linking complete!"$(Color_Off)

$(OBJECTS): $(OBJDIR)/%.o : %.cpp
	@mkdir -p $(OBJDIR)
	@$(CC) $(CFLAGS) -c $< -o $@
	@echo $(Blue)"Compiled "$(Purple)$<$(Green)" successfully!"$(Color_Off)

.PHONY: clean
clean:
	@rm -f $(OBJECTS) $(TARGET)
	@echo $(Cyan)"Cleaning is complete!"$(Color_Off)

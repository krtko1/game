#include "game.hpp"

using namespace std;

obstacle stena2 = {{1,0,0}, {{0,1,1}, {0,-1,1}, {0,-1,-1}, {0,1,-1}}};
obstacle stena = {{1,1,0}, {{-10,10,10}, {-10,10,-10}, {23,-23,-10}, {23,-23,10}}};

//openAL declaration
ALuint al_buffer, al_source[20];
ALuint al_state;
coordinates source_pos[20];
double speed = 1.;

//openGL declaration
Display *dpy;
Window root;
Window win;
GLXContext glc;
XWindowAttributes gwa;
XEvent xev;

coordinates screen_size = {1020.,680.}, pos = {0.,18.}, newpos = {0., 18.};

void mouse_t::actualize(void){
	XQueryPointer(dpy, win, &root, &root, &display.x, &display.y, &window.x, &window.y, &mask);
}

void mouse_t::debug(void){
	glColor3ub(255, 255, 255);
	draw_string(0, 0, "x: " + to_string(display.x));
	draw_string(0, 15, "y: " + to_string(display.y));
	draw_string(200, 0, "win: " + to_string(window.x));
	draw_string(200, 15, "win: " + to_string(window.y));
	draw_string(400, 15, "mask " + to_string(mask));
}

mouse_t mouse = {{0, 0}, {0, 0}, {1900, 1000*PI}, 0};

double fov = 70.0;                                 // hodnota zorneho uhlu - field of view
double nearPlane = 0.1;                            // blizsi orezavaci rovina
double farPlane = 120.0;                            // vzdalenejsi orezavaci rovina
int menu_type = MENU;
bool FRONT_MENU = true;
int button_counter = 0;
bool resolution_scrol = false;
vector <pair<coordinates, coordinates> > walls;

void savePixmap(const char *fileName) {
	unsigned char parray[gwa.height][gwa.width][3];
	GLubyte pixels[gwa.height*gwa.width*4];
	glReadPixels(0, 0, gwa.width, gwa.height, GL_RGBA, GL_UNSIGNED_BYTE, pixels);
	size_t i, j, k, cur;
    enum Constants { max_filename = 256 };
    char filename[max_filename];
    snprintf(filename, max_filename, "%s%d.ppm", fileName, 1);
    FILE *f = fopen(filename, "w");
    fprintf(f, "P3\n%d %d\n%d\n", gwa.width, gwa.height, 255);
    for (i = 0; i < gwa.height; i++) {
        for (j = 0; j < gwa.width; j++) {
            cur = 4 * ((gwa.height - i - 1) * gwa.width + j);
            fprintf(f, "%3d %3d %3d ", pixels[cur], pixels[cur + 1], pixels[cur + 2]);
        }
        fprintf(f, "\n");
    }
    fclose(f);
}

void load_texture(string name){
	ifstream file;
	string line;
	file.open(name);
	if(file.is_open()) {
		getline(file, line);
		getline(file, line);
		vector <int> line_values = line_to_ints(line);
		int width = line_values[0];
		int height = line_values[1];
		getline(file, line);
		vector <int> data = line_to_ints(line);
		GLbyte data_gl[width*height*3];
		for(int i = 0; i < data.size(); i++){
			data_gl[i] = (GLbyte)data[i];
		}
		glDrawPixels(width, height, GL_RGB, GL_BYTE, data_gl);
		file.close();
	} else cout << "Unable to open file";
}

void draw_string(double x, double y, string text, bool center){//char *string) { //font size 14
	if(center) glRasterPos3d((x * 2-9*text.size()) / screen_size.x, -(y * 2 + 14) / screen_size.y, 1);
	else glRasterPos3d(x * 2 / screen_size.x, y * 2 / screen_size.y, 1);
	for(int i = 0; i < text.size(); i++) if ((text[i])>=32) glCallList(text[i]);
	//while (*string) if ((*string)>=32) glCallList(*string++);
}

void font_init(void) {
	glPixelStorei(GL_UNPACK_ALIGNMENT, 1);
	for (int i = 32; i < 128; i++) {
		glNewList(i, GL_COMPILE);
			glBitmap(8, 16, 0.0f, 0.0f, 9.0f, 0.0f, fontbios+((i-32)<<4));
		glEndList();
	}
}

void setup_projection(void) {
	XGetWindowAttributes(dpy, win, &gwa);
	glViewport(0, 0, gwa.width, gwa.height);
	screen_size.x = gwa.width;
	screen_size.y = gwa.height;
	glClearColor(0.25, 0.25, 0.25, 1.0);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	if(menu_type) glOrtho(-1., 1., -1., 1., 1., 20.);
	else gluPerspective(fov, screen_size.x/screen_size.y, 0.0001, farPlane);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	if(menu_type) gluLookAt(0., 0., 10., 0., 0., 0., 0., 1., 0.);
	else gluLookAt(pos.x, 2.0f, pos.y,//odkial
			  pos.x+sin(mouse.look_at.x/1000.), mouse.look_at.y/1000., pos.y+cos(mouse.look_at.x/1000.),//kam
			  0., 0.0001,  0.);//orezavanie

	glDepthFunc(GL_LESS);
	glShadeModel(GL_SMOOTH);                    // nastaveni stinovaciho rezimu
    glPolygonMode(GL_FRONT, GL_FILL);           // nastaveni rezimu vykresleni modelu
    glPolygonMode(GL_BACK, GL_FILL);            // jak pro predni tak pro zadni steny
    glDisable(GL_CULL_FACE);

	ALfloat listenerOri[]={(ALfloat)sin(mouse.look_at.x/1000.), 0., (ALfloat)cos(mouse.look_at.x/1000.), 0.0,1.0,0.0};
	alListener3f(AL_POSITION, pos.x, 2.0f, pos.y);
	alListener3f(AL_VELOCITY, 0, 0, 0);
	alListenerfv(AL_ORIENTATION,listenerOri);
	//alListener3f(AL_ORIENTATION, sin(mouse.look_at.x/1000.), mouse.look_at.y/1000., cos(mouse.look_at.x/1000.));
	/*for(int i = 0; i < 20; i++){
		if(14./((source_pos[i].x-pos.x)*(source_pos[i].x-pos.x)+(source_pos[i].y-pos.y)*(source_pos[i].y-pos.y)) > 1.) alSourcef(al_source[i], AL_GAIN, 1.);
		else alSourcef(al_source[i], AL_GAIN, 14./((source_pos[i].x-pos.x)*(source_pos[i].x-pos.x)+(source_pos[i].y-pos.y)*(source_pos[i].y-pos.y)));
		alSourcef(al_source[i], AL_PITCH, speed);
	}*/
}

void exit_program(void) {
	savePixmap("pixmap.tga");
	audio_exit();
	glXMakeCurrent(dpy, None, NULL);
	glXDestroyContext(dpy, glc);
	XDestroyWindow(dpy, win);
	XCloseDisplay(dpy);
	exit(0);
}

bool key(string key_id){
	string key_string = XKeysymToString(XkbKeycodeToKeysym(dpy, xev.xkey.keycode, 0, 0));
	if (key_string == key_id) return true;
	return false;
}

void check_keyboard(void) {
    if(XCheckWindowEvent(dpy, win, KeyPressMask, &xev)) {
		if(menu_type){
			if(key("Escape")){
				if(menu_type == MENU) exit_program();
				else menu_type = MENU;
			}
		} else {
			if(key("Left") || key("a")) {
				newpos.y = pos.y - sin(mouse.look_at.x/1000.)/4;
				newpos.x = pos.x + cos(mouse.look_at.x/1000.)/4;
			} else if(key("Right") || key( "d")) {
				newpos.y = pos.y + sin(mouse.look_at.x/1000.)/4;
				newpos.x = pos.x - cos(mouse.look_at.x/1000.)/4;
			} else if(key("Up") || key("w")) {
				newpos.y = pos.y + cos(mouse.look_at.x/1000.)/2;
				newpos.x = pos.x + sin(mouse.look_at.x/1000.)/2;
			} else if(key("Down") || key("s")) {
				newpos.y = pos.y - cos(mouse.look_at.x/1000.)/4;
				newpos.x = pos.x - sin(mouse.look_at.x/1000.)/4;
			} else if(key("Escape")) {
				savePixmap("pix.tga");
				menu_type = MENU;
			}
		}
    }
}

void visible_cursor(void){
	XUndefineCursor(dpy, win);
	/*Cursor cursor;
	cursor = XCreateFontCursor(dpy, XC_left_ptr);
	XDefineCursor(dpy, win, cursor);
	XFreeCursor(dpy, cursor);*/
}

void invisible_cursor(void){
	Cursor invisibleCursor;
	Pixmap bitmapNoData;
	XColor black;
	static char noData[] = { 0,0,0,0,0,0,0,0 };
	black.red = black.green = black.blue = 0;
	bitmapNoData = XCreateBitmapFromData(dpy, win, noData, 8, 8);
	invisibleCursor = XCreatePixmapCursor(dpy, bitmapNoData, bitmapNoData, &black, &black, 0, 0);
	XDefineCursor(dpy, win, invisibleCursor);
	XFreeCursor(dpy, invisibleCursor);
	XFreePixmap(dpy, bitmapNoData);
}

void create_window(GLint width, GLint height) {
	XSetWindowAttributes swa;
	XVisualInfo *vi;
	Colormap cmap;
	int att[] = { GLX_RGBA, GLX_DEPTH_SIZE, 24, GLX_DOUBLEBUFFER, GLX_SAMPLE_BUFFERS , 1, GLX_SAMPLES , 4, None};

	dpy = XOpenDisplay(NULL);
	if(dpy == NULL) {
		printf("\n\tcannot connect to X server\n\n");
		exit(0);
	}

	root = DefaultRootWindow(dpy);
	vi = glXChooseVisual(dpy, 0, att);

	if(vi == NULL) {
		printf("\n\tno appropriate visual found\n\n");
		exit(0);
	} else printf("\n\tvisual %p selected\n", (void *)vi->visualid);

	swa.colormap = XCreateColormap(dpy, root, vi->visual, AllocNone);
	swa.event_mask = ExposureMask | KeyPressMask;
	//glfwWindowHint(GLFW_SAMPLES, 4);

	win = XCreateWindow(dpy, root, 0, 0, width, height, 0, vi->depth, InputOutput, vi->visual, CWColormap | CWEventMask, &swa);

	//invisible_cursor();

	Atom wm_state = XInternAtom(dpy, "_NET_WM_STATE", False);
    Atom fullscreen = XInternAtom(dpy, "_NET_WM_STATE_FULLSCREEN", False);

    XEvent xev;
    memset(&xev, 0, sizeof(xev));
    xev.type = ClientMessage;
    xev.xclient.window = win;
    xev.xclient.message_type = wm_state;
    xev.xclient.format = 32;
    xev.xclient.data.l[0] = 1;
    xev.xclient.data.l[1] = fullscreen;
    xev.xclient.data.l[2] = 0;

    XMapWindow(dpy, win);

   XSendEvent (dpy, DefaultRootWindow(dpy), False, SubstructureRedirectMask | SubstructureNotifyMask, &xev);
	XStoreName(dpy, win, "The game");

	glc = glXCreateContext(dpy, vi, NULL, GL_TRUE);
	glXMakeCurrent(dpy, win, glc);

	glEnable(GL_DEPTH_TEST);
	glEnable(GL_MULTISAMPLE);
	glEnable(GL_POLYGON_SMOOTH);
	glHint(GL_POLYGON_SMOOTH_HINT, GL_NICEST);
	//glEnable( GL_BLEND ) ;
	glBlendFunc( GL_SRC_ALPHA_SATURATE, GL_ONE ) ;

}

/*void fullscreen(Display* dpy, Window win) {
  Atom atoms[2] = { XInternAtom(dpy, "_NET_WM_STATE_FULLSCREEN", False), None };
  XChangeProperty(
      dpy,
      win,
      XInternAtom(dpy, "_NET_WM_STATE", False),
      XA_ATOM, 32, PropModeReplace, atoms, 1
  );
}*/

void audio_init(void){

    // Initialize the environment
    alutInit(0, NULL);

    // Capture errors
    alGetError();
}

void play_sound(){
	for (int i = 0; i < 20; i++) {
		source_pos[i] = {(double)(rand()%200-100), (double)(rand()%200-100)};
		// Load pcm data into buffer
		string name = "test0.wav";
		name[4]+= i%7;
		al_buffer = alutCreateBufferFromFile(name.c_str());
		// Create sound source (use buffer to fill source)
		alGenSources(1, &al_source[i]);
		alSourcef(al_source[i], AL_PITCH, speed);
		alSourcef(al_source[i], AL_GAIN, 1);
		alSource3f(al_source[i], AL_POSITION, source_pos[i].x, 2, source_pos[i].y);
		alSource3f(al_source[i], AL_VELOCITY, 0, 0, 0);
		alSourcei(al_source[i], AL_BUFFER, al_buffer);
		alSourcei(al_source[i], AL_LOOPING, AL_TRUE);
		// Play
		alSourcePlay(al_source[i]);
	}



}

void audio_exit(void){
    // Clean up sources and buffers
    for (int i = 0; i < 20; i++) alDeleteSources(1, &al_source[i]);
    alDeleteBuffers(1, &al_buffer);

    // Exit everything
    alutExit();
}

void rectangle(double up_x, double up_y, double down_x, double down_y ,int type, double border_r, double border_g, double border_b, double center_r, double center_g, double center_b){
	glBegin(type);
		glColor3d(center_r, center_g, center_b);
		if(type == GL_POLYGON) glVertex2d((down_x+up_x) / screen_size.x -1, 1- (down_y+up_y) / screen_size.y);
		glColor3d(border_r, border_g, border_b);
		glVertex2d(up_x*2 / screen_size.x -1, 1-up_y*2 / screen_size.y);
		glVertex2d(up_x*2 / screen_size.x -1, 1-down_y*2 / screen_size.y);
		glVertex2d(down_x*2 / screen_size.x -1, 1-down_y*2 / screen_size.y);
		glVertex2d(down_x*2 / screen_size.x -1, 1-up_y*2 / screen_size.y);
		if(type == GL_POLYGON) glVertex2d(up_x*2 / screen_size.x -1, 1-up_y*2 / screen_size.y);
	glEnd();
}

void circle(double x, double y, double radius ,GLint type) {
	double sinus, cosinus, i;
	glBegin(type);
	for(i = 0; i <= 360; i+=10) {
		sinus = sin (i * PI / 180) * radius;
		cosinus = cos (i * PI / 180) * radius / (screen_size.x/screen_size.y);
		glVertex2d(cosinus + x / (screen_size.x/screen_size.y), sinus + y);
	}
	glEnd();
}

void draw_cube(double size, double x, double y, double z) {
	glBegin(GL_QUADS);

		glColor3f(0.7, 0.0, 0.0);
		glVertex3f(-size + x, -size + z, -size + y);
		glVertex3f( size + x, -size + z, -size + y);
		glVertex3f( size + x,  size + z, -size + y);
		glVertex3f(-size + x,  size + z, -size + y);

		glColor3f(0.7, 0.7, 0.0);

		glVertex3f(-size + x, -size + z,  size + y);
		glVertex3f( size + x, -size + z,  size + y);
		glVertex3f( size + x,  size + z,  size + y);
		glVertex3f(-size + x,  size + z,  size + y);

		glColor3f(0.0, 0.0, 0.7);

		glVertex3f(-size + x, -size + z, -size + y);
		glVertex3f(-size + x, -size + z,  size + y);
		glVertex3f(-size + x,  size + z,  size + y);
		glVertex3f(-size + x,  size + z, -size + y);

		glColor3f(0.7, 0.0, 0.7);

		glVertex3f( size + x, -size + z, -size + y);
		glVertex3f( size + x, -size + z,  size + y);
		glVertex3f( size + x,  size + z,  size + y);
		glVertex3f( size + x,  size + z, -size + y);

		glColor3f(0.0, 0.7, 0.0);

		glVertex3f(-size + x, -size + z, -size + y);
		glVertex3f(-size + x, -size + z,  size + y);
		glVertex3f( size + x, -size + z,  size + y);
		glVertex3f( size + x, -size + z, -size + y);

		glColor3f(0.0, 0.7, 0.7);

		glVertex3f(-size + x, size + z, -size + y);
		glVertex3f(-size + x, size + z,  size + y);
		glVertex3f( size + x, size + z,  size + y);
		glVertex3f( size + x, size + z, -size + y);

	glEnd();
}

void draw_scene(void){

	glBegin(GL_QUADS);
		glColor3f(0.0, 0.8, 0.0);
		glVertex3f( 100., 0., 100.);
		glVertex3f(-100., 0., 100.);
		glVertex3f(-100., 0.,-100.);
		glVertex3f( 100., 0.,-100.);
	glEnd();
	for (int i = 0; i < 20; i++) {
		draw_cube(0.3, source_pos[i].x, source_pos[i].y, 0.3);
	}
	stena.draw();
	glTranslatef(0.0f, 0.5f, 0.0f);
	glRotatef(mouse.look_at.y, 1.0f, 0.0f, 0.0f);          // rotace objektu podle pohybu kurzoru mysi
	glRotatef(mouse.look_at.x, 0.0f, 1.0f, 0.0f);
	draw_cube(0.5, 0, 0, 0);
}

bool porovnaj(pair<coordinates, coordinates> a, pair<coordinates, coordinates> b){
	double vektor_aax = a.first.x-a.second.x;
	double vektor_aay = a.first.y-a.second.y;
	double vektor_ab1x = a.first.x-b.first.x;
	double vektor_ab1y = a.first.y-b.first.y;
	double vektor_ab2x = a.first.x-b.second.x;
	double vektor_ab2y = a.first.y-b.second.y;
	double c1 = vektor_aax*vektor_ab1y - vektor_aay*vektor_ab1x;
	double c2 = vektor_aax*vektor_ab2y - vektor_aay*vektor_ab2x;
	if(c1 > c2) swap(c1, c2);
	if(c1 < 0 && c2 > 0) return true;
	return false;
}

void move(void){
	bool movable = true;
	for(int i = 0; i < walls.size(); i++){
		if(porovnaj({{newpos.x, newpos.y},{pos.x, pos.y}},walls[i]) && porovnaj(walls[i], {{newpos.x, newpos.y},{pos.x, pos.y}})){
			movable = false;
		}
	}
	if(stena.colides({pos.x,0,pos.y}, {newpos.x - pos.x,0,newpos.y - pos.y})){
		movable = false;
	}
	if(movable){
		pos.x = newpos.x;
		pos.y = newpos.y;
	} else {
		newpos.x = pos.x;
		newpos.y = pos.y;
	}
}

bool button(int x, int y, string text, int width, int height){
	button_counter++;
	glColor3d(0.9, 0., 0.);
	draw_string(x, y, text);
	if(mouse.window.x <= x+screen_size.x/2+width && mouse.window.x >= x+screen_size.x/2-width && mouse.window.y <= y+screen_size.y/2+height && mouse.window.y >= y+screen_size.y/2-height){
		rectangle(x+screen_size.x/2-width, y+screen_size.y/2-height, x+screen_size.x/2+width, y+screen_size.y/2+height, GL_POLYGON, 0., 0., 0., 1., 1., 1.);
		if(mouse.mask == LEFT_CLICK && button_counter > BUTTON_COUNTER){
			button_counter = 0;
			return true;
		}
	} else rectangle(x+screen_size.x/2-width, y+screen_size.y/2-height, x+screen_size.x/2+width, y+screen_size.y/2+height, GL_POLYGON, 0., 0., 0., 0.7, 0.7, 0.7);
	return false;
}

int scrol_menu(int x, int y, string *text, int text_size, bool open, int width, int height){
	int return_value = -1;
	button_counter++;
	if(open){
		glColor3d(1.0, 0.0, 0.0);
		glBegin(GL_LINE_LOOP);
			glVertex2d(((x+width)*2+height*0.5)/screen_size.x, (y*2+height*1.5)/screen_size.y);
			glVertex2d(((x+width)*2+height*3.5)/screen_size.x, (y*2+height*1.5)/screen_size.y);
			glVertex2d((x+width+height)*2/screen_size.x, (y*2-height*1.5)/screen_size.y);
		glEnd();
		for(int i = 1; i<text_size; i++){
			glColor3d(1.,1.,1.);
			draw_string(x, y+i*height*2, text[i]);
			if(mouse.window.x <= x+screen_size.x/2+width+height*2 && mouse.window.x >= x+screen_size.x/2-width && mouse.window.y < y+screen_size.y/2+height+i*height*2 && mouse.window.y >= y+screen_size.y/2-height+i*height*2){
				rectangle(x+screen_size.x/2-width, y+screen_size.y/2-height+i*height*2, x+screen_size.x/2+width+height*2, y+i*height*2+screen_size.y/2+height, GL_POLYGON, 0., 0., 0.7, 0., 0., 0.7);
				if(mouse.mask == LEFT_CLICK && button_counter > BUTTON_COUNTER){
					return_value = i;
					button_counter = 0;
				} else return_value = 0;
			} else rectangle(x+screen_size.x/2-width, y+screen_size.y/2-height+i*height*2, x+screen_size.x/2+width+height*2, y+i*height*2+screen_size.y/2+height, GL_POLYGON, 0., 0., 0.5, 0., 0., 0.5);
		}
	} else {
		glColor3d(1.0, 0.0, 0.0);
		glBegin(GL_LINE_LOOP);
			glVertex2d(((x+width)*2+height*0.5)/screen_size.x, (y*2-height*1.5)/screen_size.y);
			glVertex2d(((x+width)*2+height*3.5)/screen_size.x, (y*2-height*1.5)/screen_size.y);
			glVertex2d((x+width+height)*2/screen_size.x, (y*2+height*1.5)/screen_size.y);
		glEnd();
	}
	glColor3d(1.,1.,1.);
	draw_string(x, y, text[0]);
	if(mouse.window.x <= x+screen_size.x/2+width+height*2 && mouse.window.x >= x+screen_size.x/2-width && mouse.window.y < y+screen_size.y/2+height && mouse.window.y >= y+screen_size.y/2-height){
		rectangle(x+screen_size.x/2-width, y+screen_size.y/2-height, x+screen_size.x/2+width, y+screen_size.y/2+height, GL_POLYGON, 0., 0.6, 0., 0., 0.6, 0.);
		rectangle(x+screen_size.x/2+width, y+screen_size.y/2-height, x+screen_size.x/2+width+height*2, y+screen_size.y/2+height, GL_POLYGON, 0., 0.7, 0., 0., 0.7, 0.);
		if(mouse.mask == LEFT_CLICK && button_counter > BUTTON_COUNTER){
			button_counter = 0;
			if(!open) return_value = 0;
		} else if(open) return_value = 0;
	} else {
		rectangle(x+screen_size.x/2-width, y+screen_size.y/2-height, x+screen_size.x/2+width, y+screen_size.y/2+height, GL_POLYGON, 0., 0.5, 0., 0., 0.5, 0.);
		rectangle(x+screen_size.x/2+width, y+screen_size.y/2-height, x+screen_size.x/2+width+height*2, y+screen_size.y/2+height, GL_POLYGON, 0., 0.6, 0., 0., 0.6, 0.);
	}
	return return_value;
}

void restart(void){
	menu_type = 0;
	FRONT_MENU = 0;
	pos.x = 0.;
	pos.y = 18.;
	mouse.look_at.y = 1900;
	mouse.look_at.x = 1000*PI;
	newpos.x = 0.;
	newpos.y = 18.;
}

void save(void){
	ofstream file;
	for(int i = 0; i < 9; i++) if(button(((i%3)-1)*220, ((i/3)-1)*220, "save " + to_string(i+1), 100, 100)){
		file.open("save"+to_string(i+1)+".exe");
		file << pos.x << '\n' << pos.y << '\n' <<	mouse.look_at.y << '\n' <<	mouse.look_at.x << '\n';
		file.close();
		menu_type = MENU;
	}
}

void load(void){
	ifstream file;
	for(int i = 0; i < 9; i++) if(button(((i%3)-1)*220, ((i/3)-1)*220, "load " + to_string(i+1), 100, 100)){
		file.open("save"+to_string(i+1)+".exe");
		file >> pos.x >> pos.y >> mouse.look_at.y >> mouse.look_at.x;
		file.close();
		menu_type = 0;
		FRONT_MENU = 0;
		newpos.x = pos.x;
		newpos.y = pos.y;
	}
}

void map_init(string name){
	ifstream file;
	string line;

	file.open(name);
	if(file.is_open()) {
		while(getline(file, line)) {
			vector <int> data = line_to_ints(line);
			walls.push_back({{(double)data[0], (double)data[1]},{(double)data[2], (double)data[3]}});
		}
		file.close();
	} else cout << "Unable to open file";
}

vector <int> line_to_ints(string line){
	vector <int> ret;
	for(int i = 0, j = 0; i < line.size(); i++, j++){
		ret.push_back(0);
		bool negative = false;
		while((line[i] >= '0' && line[i] < '9') || line[i] == '-'){
			if(line[i] == '-') negative = true;
			else {
				ret[j] *= 10;
				if(negative) ret[j] -= line[i]-'0';
				else ret[j] += line[i]-'0';
			}
			i++;
		}
	}
	return ret;
}

void draw_map(double height){
	glBegin(GL_QUADS);
	glColor3f(0.7, 0.0, 0.0);
	for(int i = 0; i < walls.size(); i++){
		glVertex3d(walls[i].first.x, 0., walls[i].first.y);
		glVertex3d(walls[i].first.x, height, walls[i].first.y);
		glVertex3d(walls[i].second.x, height, walls[i].second.y);
		glVertex3d(walls[i].second.x, 0., walls[i].second.y);
	}
	glEnd();
}

void base_menu(void){
	if(button(0, -150,"Resume")) menu_type = 0;
	if(button(0, -90,"Restart")) restart();
	if(button(0, -30,"Save")) menu_type = SAVE;
	if(button(0, 30,"Load")) menu_type = LOAD;
	if(button(0, 90,"Settings")) menu_type = SETTINGS;
	if(button(0, 150,"Exit")) exit_program();
}

void front_menu(void){
	if(button(0, -90,"New game")) restart();
	if(button(0, -30,"Load")) menu_type = LOAD;
	if(button(0, 30,"Settings")) menu_type = SETTINGS;
	if(button(0, 90,"Exit")) exit_program();
}

void settings(void){
	string text[]={"ahoj", "igooiuyr", "sfgb"};
	int a = scrol_menu(0,0,text,3, resolution_scrol);
	if(a == 0) resolution_scrol = true;
	else resolution_scrol = false;
	//cout << a << '\n';
}

void menu(void){
	visible_cursor();
	setup_projection();
	if(menu_type == MENU){
		if(FRONT_MENU) front_menu();
		else base_menu();
	}
	else if(menu_type == SAVE) save();
	else if(menu_type == LOAD) load();
	else if(menu_type == SETTINGS) settings();
	rectangle(0, 0, screen_size.x, screen_size.y, GL_POLYGON, 0.05,0.,0.,0.,0.,0.);
}

void game(void){
	check_keyboard();
	invisible_cursor();
	//if(mouse.mask == RIGHT_CLICK || mouse.mask == LEFT_CLICK)
	XWarpPointer(dpy, win, win, 0, 0, screen_size.x, screen_size.y, screen_size.x/2, screen_size.y/2);
	//if(mouse.mask == LEFT_CLICK){
		mouse.look_at.x-=(mouse.window.x-screen_size.x/2);
		mouse.look_at.y-=(mouse.window.y-screen_size.y/2);
	//}
	move();
	setup_projection();
	draw_map();
	draw_scene();


}
int main() {
	vec a = {1,2,3};
	a = -a;
	a = a*3;
	double x = 81;
	cout << (long long)RAND_MAX  << " " << rand()<< "\n";

	cout << (double)sqrt(x) << '\n';
	cout << (double)a.x << " " << (double)a.y << " " << (double)a.z << ' ' << (double)a.len() <<'\n';
	a = a.unit();
	a = priemet({1,1,1},{2,2,0});
	a = colision({0,0,0}, {2,2,2}, {0,0,1});
	cout << (double)a.x << " " << (double)a.y << " " << (double)a.z << ' ' << (a.len() == 1.) <<'\n';
	map_init("map1.exe");
	create_window(screen_size.x, screen_size.y);
	font_init();
	audio_init();
	play_sound();
	bool opak = false;
	while(1) {
		if(speed > 3) opak = true;
		if(speed < 0.6) opak = false;
		if(opak) speed -= 0.001;
		else speed += 0.001;
		mouse.actualize();
		//cout << mouse.mask << endl;
		check_keyboard();
		if(menu_type) menu();
		else game();
		//load_texture("pix.tga1.ppm");
		//mouse.debug();
		//cout << screen_size.x << screen_size.y << '\n';
		glFlush();
		glXSwapBuffers(dpy, win);
	}
}

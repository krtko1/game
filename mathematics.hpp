#ifndef MATHEMATICS_H_
#define MATHEMATICS_H_

#include <cmath>

/*std::ostream::operator<<(long double) {
	return operator<<(double);
}*/

struct vec{
	long double x;
	long double y;
	long double z;
	vec(long double x, long double y, long double z) : x(x), y(y), z(z) {}
	long double inline len(){
		return sqrt(x*x+y*y+z*z);
	}
	vec inline operator-(){
		return {-x,-y,-z};
	}
	long double inline operator^(vec b){
		return x*b.x+y*b.y+z*b.z;
	}
	vec inline operator*(vec b){
		return {y*b.z-z*b.y,
				z*b.x-x*b.z,
				x*b.y-y*b.x};
	}
	vec inline operator*(long double b){
		return {x*b, y*b, z*b};
	}
	vec inline operator/(long double b){
		return {x/b, y/b, z/b};
	}
	vec inline operator+(vec b){
		return {x+b.x, y+b.y, z+b.z};
	}
	vec inline operator-(vec b){
		return {x-b.x, y-b.y, z-b.z};
	}
	vec inline unit(){
		return *this / len();
	}
};

long double inline dot_operator(vec a, vec b){
	return a^b;
}

vec inline cross_operator(vec a, vec b){
	return a*b;
}

vec inline operator*(long double b, vec a){
	return a*b;
}

bool inline operator==(vec a, vec b){ //uplna blbost
	return abs(a.x - b.x) < 0.00000000001 && abs(a.y - b.y) < 0.0000000001 && abs(a.z - b.z) < 0.0000000001;
}

vec inline priemet(vec a, vec b){
	return (a^b.unit())*b.unit();
}

vec inline colision(vec pos, vec move, vec plane){
	vec new_pos = pos + move;
	return move + plane - priemet(new_pos, plane);
}

#endif

#ifndef OBSTACLE_H_
#define OBSTACLE_H_

#include "mathematics.hpp"
#include <vector>
#include <iostream>
#include <GL/gl.h>
#include <GL/glx.h>
#include <GL/glext.h>
#include <GL/glu.h>

struct obstacle {
	vec norm;
	std::vector <vec> point;
	void draw();
	vec inline colision(vec pos, vec move) {
		return move + norm - priemet(pos + move, norm);
	}
	bool inline colides_plane(vec pos, vec move) {
		long double s1 = pos ^ norm;
		long double s2 = norm ^ norm;
		long double s3 = (pos + move) ^ norm;
		return (s1 < s2 && s2 < s3) || (s1 > s2 && s2 > s3);
	}
	vec inline colide_pos(vec pos, vec move) { //only if colides plane
		return pos + (priemet(pos, norm) - norm).len()/(priemet(move, norm)).len()*move;
	}
	bool inline colides_polygon(vec intersect) {
		long long colides = 0;
		vec luc = norm * vec(rand(),rand(),rand());
		std::cout << luc.x << " " << luc.y << " " << luc.z << "\n";
		for(int i = 0; i < point.size(); i++) {
			vec p1 = point[i] + norm - intersect;
			vec p2 = point[(i+1)%point.size()] + norm - intersect;
			std::cout << p1.x << " " << p1.y << " " << p1.z << "\n";
			std::cout << p2.x << " " << p2.y << " " << p2.z << "\n";
			std::cout << ((p1*luc)^norm) << " " << ((p2*luc)^norm) << "\n";
			std::cout << (p1^luc) << " " << (p2^luc) << "\n";
			if(((p1*luc)^norm)*((p2*luc)^norm) < 0 && (p1.unit()^luc) + (p2.unit()^luc) > 0) //druha podmienka je bs
				colides++;
		}
		std::cout << colides << "\n";
		return colides%2;
	}
	bool inline colides(vec pos, vec move){
		if(colides_plane(pos, move))
		{
			vec is = colide_pos(pos, move);
			return colides_polygon(is);
		}
	}
};



#endif

